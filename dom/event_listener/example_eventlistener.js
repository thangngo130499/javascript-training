// Event listener

let btn = document.getElementById("btn");

// btn.addEventListener('click', function(e){
//     console.log('Event 1')
// })
// btn.addEventListener('click', function(e){
//     console.log('Event 2')
// })
// btn.addEventListener('click', function(e){
//     console.log('Event 3')
// })

function work1() {
  console.log("Work 1");
}
function work2() {
    console.log("Work 2");
  }
btn.addEventListener("click", work1);
btn.addEventListener("click", work2);

setTimeout(function () {
  btn.removeEventListener("click", work1);
}, 3000);
