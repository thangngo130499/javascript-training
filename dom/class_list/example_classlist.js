// ClassList property

var boxElement = document.querySelector(".box");

// add
boxElement.classList.add("red");

// contains
console.log(boxElement.classList.contains("red"));

// remove
setTimeout(() => {
  boxElement.classList.remove("red");
}, 3000);

// toggle
setInterval(() => {
    boxElement.classList.toggle("red");
},1000);
