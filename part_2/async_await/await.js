// Await Syntax

// async function myDisplayer() {
//   let promise = new Promise(function (resolve) {
//     resolve("Success!");
//   });
//   console.log(await promise);
// }

// myDisplayer();

// Timeout

async function myDisplayer() {
  let promise = new Promise(function (resolve) {
    setTimeout(function () {
      resolve("Success!");
    }, 3000);
    resolve("Success!");
  });
  console.log(await promise);
}

myDisplayer();
