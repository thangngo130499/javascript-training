//  Async Syntax

async function myFunction(reject) {
  return 'hello';
}

myFunction()
  .then(function (result) {
    console.log("result: ", result);
  })
  .catch(function (err) {
    console.log("Error: ", err);
  });
