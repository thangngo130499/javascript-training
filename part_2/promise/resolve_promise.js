// Promise.resolve

var promise = Promise.reject("Error!");

promise
  .then(function (result) {
    console.log("result: ", result);
  })
  .catch(function (error) {
    console.log('err: ',error);
  });
