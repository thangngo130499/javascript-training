// Callback hell
/*
setTimeout(function () {
  console.log(1);
  setTimeout(function () {
    console.log(2);
    setTimeout(function () {
      console.log(3);
      setTimeout(function () {
        console.log(4);
      }, 1000);
    }, 1000);
  }, 1000);
}, 1000);
*/

// Promise
// let promise = new Promise(
//   // Executor
//   function (resolve, reject) {
//     // Logic
//     // Success: resolve()
//     // failed: reject()
//     reject();
//   }
// );

// promise
//   .then(function () {
//     console.log("Successully!");
//   })
//   .catch(function () {
//     console.log("Failure!");
//   })
//   .finally(function () {
//     console.log("Done!");
//   });

function sleep(ms) {
  return new Promise(function (resolve) {
    setTimeout(resolve, ms);
  });
}

sleep(1000)
  .then(function () {
    console.log(1);
    return sleep(1000);
  })
  .then(function () {
    console.log(2);
    return new Promise(function (resolve, reject) {
      reject('Error!');
    });
  })
  .then(function () {
    console.log(3);
    return sleep(1000);
  })
  .then(function () {
    console.log(4);
    return sleep(1000);
  })
  .catch(function (err) {
    console.log(err);
  });
