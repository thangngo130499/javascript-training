// Comparison Operators
var a = 1;
var b = 2;

if (a > b) {
  console.log("True condition");
} else {
  console.log("False condition");
}

if (a < b) {
  console.log("True condition");
} else {
  console.log("False condition");
}

if (a == b) {
  console.log("True condition");
} else {
  console.log("False condition");
}

if (a != b) {
  console.log("True condition");
} else {
  console.log("False condition");
}
if (a >= b) {
  console.log("True condition");
} else {
  console.log("False condition");
}
if (a <= b) {
  console.log("True condition");
} else {
  console.log("False condition");
}
