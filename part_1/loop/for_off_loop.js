// for/off loop

var languages = ["JavaScript", "PHP", "Java", "Ruby"];
var languages2 = "javascript";

var languages3 = {
  name: "Thang Ngo",
  age: 22,
};

// for (var value of languages3) {
//   console.log(value);
// }

for (var value of Object.keys(languages3)) {
  console.log(value);
}

for (var value of Object.values(languages3)) {
  console.log(value);
}

// console.log(Object.keys(languages3))
