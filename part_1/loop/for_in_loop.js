// for/in loop

var myInfo = {
    name: 'Thang Ngo',
    age: '22',
    address: 'Da Nang, VN'
};

var languages = [
    'Javascript',
    'PHP',
    'Java',
    'Dart',
]

var myString = 'learn JavaScript';


// for (var key in myInfo) {
//     console.log(key);
// }

for (var key in myString) {
    console.log(myString[key]);
}

// for (var key in languages) {
//     console.log(languages[key]);
// }
