// Parameters in function

// function writeLog(message, message2){
//     console.log(message);
//     console.log(message2);
// }

// writeLog('Test message', 'Test message2');

// function writeLog(message, message2) {
//   if (message) {
//     console.log(message);
//   }
//   if (message2) {
//     console.log(message2);
//   }
// }

// writeLog("Test message", "Test message2");

// Arguments object

// function writeLog() {
//   console.log(arguments);
// }

//writeLog("log 1", "log 2");

// for of

// function writeLog() {
//   for (var param of arguments) {
//     console.log(param);
//   }
// }

function writeLog() {
    var myString = '';
    for (var param of arguments) {
      myString += `${param} - `
    }
    console.log(myString);
  }

writeLog("log 1", "log 2");
