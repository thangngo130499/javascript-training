// Logical operators
var a = 1;
var b = 2;
var c = 3;

if(a > 0 && b>0){
    console.log('true condition')
}


if (a > 0 || b < 0) {
  console.log("true condition");
}


if (a > 0 || b < 0 || c > 0) {
  console.log("true condition");
}


if (!(a < 0)) {
  console.log("true condition");
}
