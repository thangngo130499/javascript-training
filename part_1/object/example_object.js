// Object

var emailKey = 'email';

var myInfo = {
  name: "Thang Ngo",
  age: 23,
  address: "Da Nang, VN",
  [emailKey]: 'thangngo@gmail.com',
  getName: function(){
      return this.name;
  }
};

// myInfo.email = 'thangngo130499@gmail.com';

// var myKey = 'address';

// delete
// delete myInfo.name;

console.log(myInfo.getName());
