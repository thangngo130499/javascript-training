// Object prototype

function User(firstName, lastName, avatar) {
  this.firstName = firstName;
  this.lastName = lastName;
  this.avatar = avatar;
  this.getName = function () {
    return `${this.firstName} ${this.lastName}`;
  };
}

User.prototype.className = "F8";
User.prototype.getClassName = function () {
  return this.className;
};

var user = new User("Thang", "Ngo", "Avatar");
var user2 = new User("Toan", "Truong", "Avatar");

(user.title = "share"), (user2.comment = "hello word");

console.log(user.className);
console.log(user2.getClassName());
