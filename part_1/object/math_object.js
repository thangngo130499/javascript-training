// Math object

// Math.PI
console.log(Math.PI);

// Math.round()
console.log(Math.round(1.3));
console.log(Math.round(1.8));

// Math.abs()
console.log(Math.abs(-4));

// Math.ceil()
console.log(Math.ceil(4.0005));

// Math.floor()
console.log(Math.floor(5.9));

// Math.random()
console.log(Math.random());
console.log(Math.floor(Math.random() * 10));

var random = Math.floor(Math.random() * 100);
/*
var bonus = [
    '10 coin',
    '20 coin',
    '30 coin',
    '40 coin',
    '50 coin',
]

console.log(bonus[random])
*/

if (random < 50) {
  console.log("success");
}

// Math.min()
console.log(Math.min(-100, 1, 90, 50, 40));

// Math.max()
console.log(Math.max(-100, 1, 90, 50, 40));
