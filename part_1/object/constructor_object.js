// Object constructor

function User(firstName, lastName, avatar) {
  this.firstName = firstName;
  this.lastName = lastName;
  this.avatar = avatar;

  this.getName = function () {
    return `${this.firstName} ${this.lastName}`;
  };
}

var author = new User("Thang", "Ngo", "Avatar");
var user = new User("Toan", "Truong", "Avatar");

(author.title = "share"), (user.comment = "hello word");

console.log(author.getName());
console.log(user);
