// Conditionals

var isSuccess = 1 < 2;
if (isSuccess) {
  console.log("true condition");
} else {
  console.log("false condition");
}

var fullName = 'Thang Ngo';
if (fullName) {
  console.log("true condition");
} else {
  console.log("false condition");
}

if (null) {
  console.log("true condition");
} else {
  console.log("false condition");
}
